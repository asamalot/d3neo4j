import React from "react";
import "./App.css";
import GraphVisualizationComponent from "./components/GraphVisualizationComponent";
import TabularComponent from "./components/TabularComponent";
import Navbar from "react-bootstrap/Navbar";
import LoanForm from "./components/LoanForm";

function App() {
  return (
    <div className="App">
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>Fraud Detection</Navbar.Brand>
      </Navbar>
      <GraphVisualizationComponent />
      <TabularComponent />
      <LoanForm />
    </div>
  );
}

export default App;

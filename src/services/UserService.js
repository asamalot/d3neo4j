import axios from 'axios'

const USERS_REST_API_URL = "http://localhost:3000/ring";
const GRAPH_REST_API_URL = "http://localhost:3000/fraud";
const LOAN_REST_API_URL = "http://localhost:3000/loan";


class UserService{

    getUsers(){
        return axios.get(USERS_REST_API_URL);
    }

    getGraph(){
        return axios.get(GRAPH_REST_API_URL);
    }

    postMaLoan(loan){
        axios.post(LOAN_REST_API_URL, loan).then(response=>{
            console.log(response)
        })
        .catch(error=>{
            console.log(error)
        })
    }
    
}

export default new UserService();
import React from "react";
import Button from "react-bootstrap/Button";
import UserService from "../services/UserService";

class QueryComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    UserService.getUsers().then((response) => {
      this.setState({
        users: response.data, // Remove this when there is actually an endpoint
        query: `
                MATCH (n:Character)-[:INTERACTS1]->(m:Character) 
                RETURN n.name as source, m.name as target
                `,
      });
    });
  }

  render() {
    return (
      <div class="row justify-content-center">
        <div class="col-8">
          <textarea
            style={{ display: "block", width: "800px", height: "100px" }}
            value={this.state.query}
          ></textarea>
        </div>
        <div class="col-4">
          <Button style={{ margin: "30px" }} variant="primary">
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default QueryComponent;

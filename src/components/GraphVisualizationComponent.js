import React, { useState, useEffect } from "react";
import UserService from "../services/UserService";
import { ForceGraph } from "./forceGraph";

function UserComponent() {
  const [data, setGraph] = useState({ nodes: [], links: [] });

  useEffect(() => {
    UserService.getGraph().then((response) => {
      setGraph(response.data);
    });
  }, []);

  const nodeHoverTooltip = React.useCallback((node) => {
    let hoverDetails = `
       <div>${node.properties}</div>
        `;

    if (node.labels[0] === "AccountHolder") {
      hoverDetails = `
            <div>First Name: ${node.properties.FirstName}</div>
            <div>Last Name: ${node.properties.LastName}</div>
            `;
    }
    if (node.labels[0] === "CreditCard") {
      hoverDetails = `
            <div>Account #: ${node.properties.AccountNumber}</div>
            <div>Balance: ${node.properties.Balance}</div>
            `;
    }
    if (node.labels[0] === "SSN") {
      hoverDetails = `
            <div>Social Security #: ${node.properties.SSN}</div>
            `;
    }
    if (node.labels[0] === "UnsecuredLoan") {
      hoverDetails = `
            <div>Loan Amount:${node.properties.LoanAmount}</div>
            <div>Balance: ${node.properties.Balance}</div>
            `;
    }
    if (node.labels[0] === "BankAccount") {
      hoverDetails = `
            <div>Account #: ${node.properties.AccountNumber}</div>
            <div>Balance: ${node.properties.Balance}</div>
            `;
    }
    if (node.labels[0] === "PhoneNumber") {
      hoverDetails = `
            <div>Phone #${node.properties.PhoneNumber}</div>
            `;
    }
    if (node.labels[0] === "Address") {
      hoverDetails = `
            <div>Street: ${node.properties.Street}</div>
            <div>City: ${node.properties.City}</div>
            `;
    }

    return hoverDetails;
  }, []);

  return (
    <div>
      {data.links != "" ? (
        <ForceGraph
          linksData={data.links}
          nodesData={data.nodes}
          nodeHoverTooltip={nodeHoverTooltip}
        />
      ) : (
        <h1>Waiting for data</h1>
      )}
    </div>
  );
}

export default UserComponent;

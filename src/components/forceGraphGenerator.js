import * as d3 from "d3";
import "@fortawesome/fontawesome-free/css/all.min.css";
import styles from "./forceGraph.module.css";

export function runForceGraph(
  container,
  linksData,
  nodesData,
  nodeHoverTooltip
) {
  const links = linksData.map((d) => Object.assign({}, d));
  const nodes = nodesData.map((d) => Object.assign({}, d));
  const containerRect = container.getBoundingClientRect();
  const height = containerRect.height;
  const width = containerRect.width;

  const color = (d) => {
    let colorChoice = "#000000";
    if (d.labels[0] === "AccountHolder") {
      colorChoice = "#90EE90";
    }
    if (d.labels[0] === "CreditCard") {
      colorChoice = "#FFD700";
    }
    if (d.labels[0] === "SSN") {
      colorChoice = "#CD5C5C";
    }
    if (d.labels[0] === "UnsecuredLoan") {
      colorChoice = "#778899";
    }
    if (d.labels[0] === "BankAccount") {
      colorChoice = "#FF4500";
    }
    if (d.labels[0] === "PhoneNumber") {
      colorChoice = "#800080";
    }
    if (d.labels[0] === "Address") {
      colorChoice = "#40E0D0";
    }

    return colorChoice;
  };

  const icon = (d) => {
    let nodeLabel = " ";

    if (d.labels[0] === "AccountHolder") {
      nodeLabel = d.properties.FirstName;
    }
    if (d.labels[0] === "CreditCard") {
      // nodeLabel = d.properties.AccountNumber
      nodeLabel = "CC";
    }
    if (d.labels[0] === "SSN") {
      // nodeLabel = d.properties.SSN
      nodeLabel = "SSN";
    }
    if (d.labels[0] === "UnsecuredLoan") {
      // nodeLabel = d.properties.LoanAmount
      nodeLabel = "$";
    }
    if (d.labels[0] === "BankAccount") {
      // nodeLabel = d.properties.Balance
      nodeLabel = "$";
    }
    if (d.labels[0] === "PhoneNumber") {
      // nodeLabel = d.properties.PhoneNumber
      nodeLabel = "#";
    }
    if (d.labels[0] === "Address") {
      // nodeLabel = d.properties.Zip
      nodeLabel = "Loc.";
    }
    return nodeLabel;
  };

  const getClass = (d) => {
    return d.properties.FirstName === "John" ? styles.male : styles.female;
  };

  const drag = (simulation) => {
    const dragstarted = (d) => {
      if (!d3.event.active) simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    };

    const dragged = (d) => {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    };

    const dragended = (d) => {
      if (!d3.event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    };

    return d3
      .drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended);
  };

  // Add the tooltip element to the graph
  const tooltip = document.querySelector("#graph-tooltip");
  if (!tooltip) {
    const tooltipDiv = document.createElement("div");
    tooltipDiv.classList.add(styles.tooltip);
    tooltipDiv.style.opacity = "0";
    tooltipDiv.id = "graph-tooltip";
    document.body.appendChild(tooltipDiv);
  }
  const div = d3.select("#graph-tooltip");

  const addTooltip = (hoverTooltip, d, x, y) => {
    div.transition().duration(200).style("opacity", 0.9);
    div
      .html(hoverTooltip(d))
      .style("left", `${x}px`)
      .style("top", `${y - 28}px`);
  };

  const removeTooltip = () => {
    div.transition().duration(200).style("opacity", 0);
  };

  const simulation = d3
    .forceSimulation(nodes)
    .force(
      "link",
      d3.forceLink(links).id((d) => d.id)
    )
    .force("charge", d3.forceManyBody().strength(-750))
    .force("x", d3.forceX())
    .force("y", d3.forceY());

  const svg = d3
    .select(container)
    .append("svg")
    .attr("viewBox", [-width / 2, -height / 2, width, height])
    .call(
      d3.zoom().on("zoom", function () {
        svg.attr("transform", d3.event.transform);
      })
    );

  // define arrow markers for graph links
  svg
    .append("svg:defs")
    .append("svg:marker")
    .attr("id", "end-arrow")
    .attr("viewBox", "0 -5 10 10")
    .attr("refX", 10)
    .attr("markerWidth", 20)
    .attr("markerHeight", 20)
    .attr("orient", "auto")
    .append("svg:path")
    .attr("d", "M0,-5L20,0L0,5")
    .attr("fill", "#000");

  const link = svg
    .append("g")
    .attr("stroke", "#999")
    .attr("stroke-opacity", 0.6)
    .selectAll("line")
    .data(links)
    .join("line")
    .attr("stroke-width", (d) => Math.sqrt(d.value))
    .attr("marker-end", "url(#end-arrow)");

  var linkText = svg
    .append("g")
    .selectAll("text")
    .data(links)
    .enter()
    .append("text")
    .attr("class", "link-label")
    .attr("font-family", "Arial, Helvetica, sans-serif")
    .attr("fill", "Black")
    .style("font", "normal 12px Arial")
    .attr("dy", ".35em")
    .attr("text-anchor", "middle")
    .text(function (d) {
      return d.type.substring(4);
    });

  const node = svg
    .append("g")
    .attr("stroke", "#fff")
    .attr("stroke-width", 2)
    .selectAll("circle")
    .data(nodes)
    .join("circle")
    .attr("r", 15)
    .attr("fill", color)
    .call(drag(simulation));

  const label = svg
    .append("g")
    .attr("class", "labels")
    .selectAll("text")
    .data(nodes)
    .enter()
    .append("text")
    .attr("text-anchor", "middle")
    .attr("dominant-baseline", "central")
    .attr("class", (d) => `fa ${getClass(d)}`)
    .text((d) => {
      return icon(d);
    })
    .call(drag(simulation));

  label
    .on("mouseover", (d) => {
      addTooltip(nodeHoverTooltip, d, d3.event.pageX, d3.event.pageY);
    })
    .on("mouseout", () => {
      removeTooltip();
    });

  simulation.on("tick", () => {
    //update link positions
    link
      .attr("x1", (d) => d.source.x)
      .attr("y1", (d) => d.source.y)
      .attr("x2", (d) => d.target.x)
      .attr("y2", (d) => d.target.y);

    linkText
      .attr("x", function (d) {
        return (d.source.x + d.target.x) / 2;
      })
      .attr("y", function (d) {
        return (d.source.y + d.target.y) / 2;
      });

    // update node positions
    node.attr("cx", (d) => d.x).attr("cy", (d) => d.y);

    // update label positions
    label
      .attr("x", (d) => {
        return d.x;
      })
      .attr("y", (d) => {
        return d.y;
      });
  });

  return {
    destroy: () => {
      simulation.stop();
    },
    nodes: () => {
      return svg.node();
    },
  };
}

import React from 'react'
import { useFormik } from 'formik'
import UserService from "../services/UserService";

function LoanForm(){
    const formik = useFormik({
        initialValues:{
            name: '',
            email: '',
        },
        onSubmit: values=>{
            console.log('Submit values', values)
            UserService.postMaLoan(values);
        }
    })
    // console.log('Form valu es', formik.values)

    return(
        <div>
            <form onSubmit={formik.handleSubmit}>
                <label htmlFor='name'>Name</label>
                <input 
                type='text'
                id='name'
                name='name'
                onChange={formik.handleChange}
                value={formik.values.name}
                />
                <label htmlFor='email'>Email</label>
                <input 
                type='email'
                id='email'
                name='email'
                onChange={formik.handleChange}
                value={formik.values.email}
                />
                <button>Submit</button>
            </form>
        </div>
    )
}

export default LoanForm
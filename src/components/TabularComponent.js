import React, { useState, useEffect } from "react";
import UserService from "../services/UserService";

function TabularComponent() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    UserService.getUsers().then((response) => {
      setUsers(response.data);
    });
  }, []);

  return (
    <div>
      {users != "" ? (
        <table className="table table-striped">
          <thead>
            <tr>
              <td>Fraud Ring</td>
              <td>Contact Type</td>
              <td>Ring Size</td>
              <td>Financial Risk</td>
            </tr>
          </thead>

          <tbody>
            {users.map((user) => (
              <tr key={user.FinancialRisk}>
                <td>{user.FraudRing}</td>
                <td>{user.ContactType}</td>
                <td>{user.RingSize}</td>
                <td>{user.FinancialRisk}</td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <h1>Waiting for data</h1>
      )}
    </div>
  );
}

export default TabularComponent;
